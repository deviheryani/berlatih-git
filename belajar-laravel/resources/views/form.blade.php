@extends('master')

@section('konten')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


<section class="content">

<div class="container-fluid">
        <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Buat Account Baru</h3>
            </div>

            <form name="pendaftaran" action="/welcome" method="post">
            @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="first_name" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="last_name" required>
                  </div>

                  <div class="form-group">
                      <label for="exampleInputEmail1">Gender</label>
                      <div class="form-check">
                          <input class="form-check-input" type="radio" name="gender">
                          <label class="form-check-label">Male</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="gender">
                          <label class="form-check-label">Female</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="gender">
                          <label class="form-check-label">Other</label>
                        </div>
                  </div>
                  <div class="form-group">
                        <label>Nationality</label>
                        <select class="custom-select" name="nationality">
                          <option>Indonesia</option>
                          <option>Malaysia</option>
                          <option>Singapora</option>
                          <option>Brunie</option>
                        </select>
                  </div>
                  <div class="form-group">
                    <label>Language Spoken</label>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Bahasa Indonesia</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Bahasa Inggris</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Arabic</label>
                        </div>
                  </div>

                  <div class="form-group">
                        <label>Bio</label>
                        <textarea class="form-control" rows="3" placeholder="Bio"></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>

        </div>
        </div>    
    </div>
</div><!-- /.container-fluid -->
</section>

@endsection