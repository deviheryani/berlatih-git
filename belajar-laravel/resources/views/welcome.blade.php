@extends('master')

@section('konten')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


<section class="content">

<div class="container-fluid">
        <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Welcome</h3>
            </div>

            <h5 class="md-2">Selamat Datang {{$fn}} {{$ln}} </h2>
            <h5 class="md-2">Terimakasih telah bergabung dengan SanberBook. Media Belajar kita bersama</h5>

        </div>
        </div>    
    </div>
</div><!-- /.container-fluid -->
</section>

@endsection