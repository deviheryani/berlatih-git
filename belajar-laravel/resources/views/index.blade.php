@extends('master')



@section('konten')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


<section class="content">
<div class="container-fluid">
        <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
            </div>
            <h1>SanberBook</h1><br>
            <h3>Sosial Media Developer Berkualitas</h3>
            <p>Belajar dan Berbagi agar hidup lebih baik <br><br>
            <b>Benefit join di SanberBook</b><br>
            <ul>
                <li>Mendapatkan motivasi dari sesama para Developer</li>
                <li>Sharing Knowledge</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
            <b>Cara bergabung ke SanberBook</b><br>
            <ol>
                <li>Mengunjungi Website Ini</li>
                <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
                <li>Selesai</li>
            </ol>
            </p>        
            </div>    
</div>
        </div>
</div><!-- /.container-fluid -->
</section>

@endsection