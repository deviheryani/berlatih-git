<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran', function (Blueprint $table) {

            $table->id()->unique();
            $table->string('keterangan');
            $table->unsignedBigInteger('cast_id');
            $table->unsignedBigInteger('id_film');            
            $table->foreign('cast_id')->references('id')->on('cast');
            $table->foreign('id_film')->references('id')->on('film');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
    }
}
