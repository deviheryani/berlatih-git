<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }

    public function welcome(Request $request) {

        $fn = $request->first_name;
        $ln = $request->last_name;
        return view('welcome', ['fn' => $fn, 'ln' => $ln]);
        //return view('welcome');
    }
}