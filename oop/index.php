<?php

require("animal.php");
require("Ape.php");
require("Frog.php");

$sheep = new animal("Shaun");

echo "Name : " . $sheep->name;
echo "<br>";
echo "Legs : " . $sheep->legs;
echo "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded;
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->name;
echo "<br>";
echo "Legs : " . $sungokong->legs_ape;
echo "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded;
echo "<br>";
$sungokong->yell();
echo "<br>";
echo "<br>";

$kodok = new Frog("buduk");

echo "Name : " . $kodok->name;
echo "<br>";
echo "Legs : " . $kodok->legs;
echo "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded;
echo "<br>";
$kodok->jump();

?>